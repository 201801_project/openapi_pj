package net.skhu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.skhu.dto.Basic;
import net.skhu.dto.Cooking;
import net.skhu.dto.Ingredient;
import net.skhu.mapper.AreaMapper;
import net.skhu.mapper.ArticleMapper;
import net.skhu.mapper.BasicMapper;
import net.skhu.mapper.BoardMapper;
import net.skhu.mapper.CalenderMapper;
import net.skhu.mapper.CookingMapper;
import net.skhu.mapper.FileMapper;
import net.skhu.mapper.IngredientMapper;
import net.skhu.mapper.Match2Mapper;
import net.skhu.mapper.TimetableMapper;
import net.skhu.mapper.UserMapper;
import net.skhu.model.Option;
import net.skhu.model.Pagination;
import net.skhu.model.Select;
import net.skhu.service.NaverBookService;
import net.skhu.service.UserService;

@Controller
@RequestMapping("memb")
public class MemberController {

	@Autowired
	BasicMapper basicMapper;
	@Autowired
	IngredientMapper ingredientMapper;
	@Autowired
	CookingMapper cookingMapper;
	@Autowired
	ArticleMapper articleMapper;
	@Autowired
	UserMapper userMapper;
	@Autowired
	BoardMapper boardMapper;
	@Autowired
	UserService userService;
	@Autowired
	CalenderMapper calenderMapper;
	@Autowired
	Match2Mapper match2Mapper;
	@Autowired
	FileMapper fileMapper;
	@Autowired
	TimetableMapper timetableMapper;
	@Autowired
	AreaMapper areaMapper;
	NaverBookService service;

	@RequestMapping(value = "dynamicSQL2")				//메뉴 '기본' 정보 조회
	public String dynamicSQL2(Pagination pagination, Model model) {
		int count = basicMapper.count(pagination);
		pagination.setRecordCount(count);

		List<Basic> list = basicMapper.findAll(pagination);
		model.addAttribute("list", list);

		Option[] orderBy = basicMapper.orderBy;
		Option[] searchBy = basicMapper.searchBy;

		model.addAttribute("orderBy", orderBy);
		model.addAttribute("searchBy", searchBy);

		return "guest/dynamicSQL2";

	}

	@RequestMapping(value = "igr", method = RequestMethod.GET)	//메뉴 '재료' 정보 조회
	public String igr(Model model, @RequestParam("recipe_id") int id) {
		List<Ingredient> ingredients = ingredientMapper.findByRecipeId(id);
		model.addAttribute("ingredients", ingredients);
		return "memb/igr";
	}

	@RequestMapping(value = "slide", method = RequestMethod.GET)
	public String slide(/* Pagination slide, */ Model model, @RequestParam("recipe_id") int id) {

		// int count = cookingMapper.count(slide);
		// slide.setRecordCount(count);

		List<Cooking> list = cookingMapper.findByRecipeId(/* slide, */ id);
		model.addAttribute("list", list);

		return "memb/slide";
	}

	@RequestMapping(value = "index")
	public String index() {
		return "memb/index";
	}

	/*@RequestMapping(value = "calender")
	public String calender(Model model) {
		List<Calender> list = calenderMapper.findByMonth(2);
		model.addAttribute("list",list);
	/*
	 * @RequestMapping(value="result", method = RequestMethod.POST) public String
	 * delete(Model model, Select select ) { String text1= select.getText1(); String
	 * radio1=select.getRadio1(); List<Basic> list =
	 * basicMapper.findByCalorieAndNationnm(text1,radio1);
	 * model.addAttribute("list",list);
	 *
	 *
	 *
	 return "memb/result";

	 }*/

	@RequestMapping(value = "select")
	public String select() {
		return "memb/select";
	}

	/*@RequestMapping(value = "result")
	public String go(Model model, @RequestParam("unit") List<String> unit) {

		List<Basic> list = basicMapper.findByTynmIn(unit);
		model.addAttribute("list", list);

		return "memb/result";

	}*/
	@RequestMapping(value = "result")
	public String go(Model model, Select select) {
		if(select.getRadio2() == 0) {
			List<Basic> fc = basicMapper.findByCalorie0(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 1) {
			List<Basic> fc = basicMapper.findByCalorie1(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 2) {
			List<Basic> fc = basicMapper.findByCalorie2(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 3) {
			List<Basic> fc = basicMapper.findByCalorie3(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 4) {
			List<Basic> fc = basicMapper.findByCalorie4(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 5) {
			List<Basic> fc = basicMapper.findByCalorie5(select);
			model.addAttribute("fc", fc);
		}
		else if(select.getRadio2() == 6) {
			List<Basic> fc = basicMapper.findByCalorie6(select);
			model.addAttribute("fc", fc);
		}
		//List<Basic> list = basicMapper.findBySelect(select);
		//model.addAttribute("list", list);
		return "memb/result";

	}






}