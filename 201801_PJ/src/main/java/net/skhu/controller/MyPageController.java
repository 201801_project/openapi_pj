package net.skhu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.skhu.dto.Area;
import net.skhu.dto.Article;
import net.skhu.dto.Match2;
import net.skhu.dto.Timetable;
import net.skhu.dto.User;
import net.skhu.mapper.AreaMapper;
import net.skhu.mapper.ArticleMapper;
import net.skhu.mapper.Match2Mapper;
import net.skhu.mapper.TimetableMapper;
import net.skhu.mapper.UserMapper;
import net.skhu.model.Option;
import net.skhu.model.TimeCompare;
import net.skhu.model.TimeSelect;

@Controller
@RequestMapping("memb")
public class MyPageController {
	@Autowired
	UserMapper userMapper;

	@Autowired
	AreaMapper areaMapper;
	@Autowired
	Match2Mapper match2Mapper;
	@Autowired
	ArticleMapper articleMapper;
	@Autowired
	TimetableMapper timetableMapper;

	@RequestMapping(value = "mypage") // 마이페이지
	public String mypage(Model model, Authentication auth) {
		String vo = (String) auth.getPrincipal(); // 현재로그인 된 사용자 아이디
		User mypage = userMapper.findOneByLoginId(vo); // 로그인 아이디로 유저 정보를 가져옴
		int area_id = mypage.getArea_id();
		Area area = areaMapper.findOneByAreaId(area_id);

		String area_name = area.getArea_name(); // 지역이름

		model.addAttribute("area_name", area_name);
		model.addAttribute("mypage", mypage);
		return "memb/mypage";

	}

	@RequestMapping(value = "mymatch2") // 내가 개설한 요리교실 내역 조회
	public String match2(Model model, Authentication auth) {
		String vo = (String) auth.getPrincipal();
		List<Match2> list = match2Mapper.findByLoginId(vo);

		model.addAttribute("list", list);

		return "memb/matchCommunity";

	}

	@RequestMapping(value = "mycommu") // 내가 쓴 커뮤니티 글 조회
	public String mycommu(Model model, Authentication auth) {
		String vo = (String) auth.getPrincipal();
		List<Article> CommuList = articleMapper.findByLoginId(vo);

		model.addAttribute("CommuList", CommuList);

		return "memb/community";

	}

	@RequestMapping(value = "mycomment") // 내가 쓴 댓글 게시판별 그리고 글 별로 조회
	public String mycomment(Model model, Authentication auth) {
		String vo = (String) auth.getPrincipal();
		List<Article> articles = articleMapper.findAllWithComments(vo); // 커뮤니티 댓글
		List<Match2> matches = match2Mapper.findAllWithComments(vo); // 요리교실 댓글
		model.addAttribute("articles", articles);
		model.addAttribute("matches", matches);

		return "memb/commentList";

	}

	@RequestMapping(value = "myarea") // 설정 지역 수정 페이지
	public String editarea(Model model, Authentication auth) {
		Option[] area = UserMapper.area;
		User user = new User();
		model.addAttribute("area", area);
		model.addAttribute("user", user);
		return "memb/editarea";

	}

	@RequestMapping(value = "editarea", method = RequestMethod.GET) // 지역 수정하기
	public String editArea(Model model, Authentication auth, @RequestParam("area_id") int area_id) {
		String vo = (String) auth.getPrincipal();
		User user = userMapper.findOneByLoginId(vo);
		userMapper.update(area_id, vo);

		model.addAttribute("mypage", user);
		return "redirect:mypage";

	}

	@RequestMapping(value = "deleteAccount", method = RequestMethod.GET) // 탈퇴하기
	public String deleteAccount(Model model, Authentication auth) {
		String login_id = (String) auth.getPrincipal();
		userMapper.deleteByLoginId(login_id);

		return "guest/bye"; // logout프로세싱을 위한 페이지로 이동

	}

	@RequestMapping(value = "timetable")
	public String timetable() {
		return "memb/timetable";
	}

	@RequestMapping(value = "timeselect")
	public String timeselect(Model model, TimeSelect timeselect, Authentication auth) {
		timetableMapper.update(timeselect);
		String vo = (String) auth.getPrincipal();
		User user = userMapper.findOneByLoginId(vo);
		model.addAttribute("mypage", user);
		return "memb/mypage";

	}

	@RequestMapping(value = "timecreate")
	public String timecreate(Model model, TimeSelect timeselect) {
		timetableMapper.insert(timeselect);

		return "memb/availtime";

	}

	@RequestMapping(value = "timetest", method = RequestMethod.GET)
	public String test(Model model, Authentication auth) {
		TimeSelect timeselect = new TimeSelect();
		model.addAttribute(timeselect);

		String vo = (String) auth.getPrincipal();

		model.addAttribute("vo", vo);
		return "memb/availtime";

	}

	@RequestMapping(value = "timecompare")
	public String timecompare(Model model, TimeCompare timecompare) {

		List<Timetable> mon = timetableMapper.findByUserAndMon(timecompare);
		List<Timetable> tue = timetableMapper.findByUserAndTue(timecompare);
		List<Timetable> wed = timetableMapper.findByUserAndWed(timecompare);
		List<Timetable> thu = timetableMapper.findByUserAndThu(timecompare);
		List<Timetable> fri = timetableMapper.findByUserAndFri(timecompare);

		model.addAttribute("mon", mon);
		model.addAttribute("tue", tue);
		model.addAttribute("wed", wed);
		model.addAttribute("thu", thu);
		model.addAttribute("fri", fri);

		return "memb/timecompare";

	}

}
