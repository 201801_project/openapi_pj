package net.skhu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.skhu.dto.Area;
import net.skhu.dto.Calender;
import net.skhu.dto.Match2;
import net.skhu.dto.Schedule;
import net.skhu.dto.User;
import net.skhu.mapper.AreaMapper;
import net.skhu.mapper.CalenderMapper;
import net.skhu.mapper.Match2Mapper;
import net.skhu.mapper.ScheduleMapper;
import net.skhu.mapper.UserMapper;
import net.skhu.model.CreateMatch2Article;


@Controller
@RequestMapping("memb")
public class ProChefController {

	@Autowired
	AreaMapper areaMapper;
	@Autowired
	ScheduleMapper scheduleMapper;
	@Autowired
	CalenderMapper calenderMapper;
	@Autowired
	Match2Mapper match2Mapper;
	@Autowired
	UserMapper userMapper;


	@RequestMapping(value = "createMatch", method = RequestMethod.GET) 				// 요리교실 생성 페이지
	public String insert( Model model, Authentication auth, @RequestParam("area_id") int area_id) {
		CreateMatch2Article createMatch2Article = new CreateMatch2Article();
		String vo = (String) auth.getPrincipal();
		Area area = areaMapper.findOneByAreaId(area_id);
		List<Schedule> schedules = scheduleMapper.findAll();
		model.addAttribute("area", area);											//지역 목록
		model.addAttribute("schedules", schedules);									//시각 목록
		model.addAttribute("vo", vo);
		model.addAttribute("createMatch2Article", createMatch2Article);
		model.addAttribute("calenderMonths", calenderMapper.findDistinctMonth());	//월 목록
		model.addAttribute("calenderDates", calenderMapper.findDistinctDate());		//일 목록

		return "memb/createMatch";
	}

	@RequestMapping(value = "createMatch", method = RequestMethod.POST) 			// 요리교실 생성
	public String insert(CreateMatch2Article createMatch2Article, Model model) {
		match2Mapper.insert(createMatch2Article);
		int month = createMatch2Article.getMonth();
		List<Match2> exists = match2Mapper.findExist(month);
		List<Calender> list = calenderMapper.findByEntire(month);					//요리교실 개설할때 선택한 월을 기준으로 달력의 월이 표시됨
		model.addAttribute("exists", exists);
		model.addAttribute("list", list);
		model.addAttribute("month", month);
		return "memb/calender";
	}

	@RequestMapping(value = "userList") 											// 관리자 기능, 회원 목록 조회
	public String userList( Model model ) {
		List<User> list = userMapper.findAll();
		User u = new User();														//모달창 수정을 위한 객체
		List<Area> area = areaMapper.findAll();
		model.addAttribute("u",u);
		model.addAttribute("list", list);
		model.addAttribute("area", area);

		return "admin/userList";
	}

	@RequestMapping(value = "updateAll",  method = RequestMethod.POST)
	public String userUpdate( Model model, User u) {								//모달창에서 수정하기
		userMapper.updateAll(u);


		return "redirect:userList";
	}

	@RequestMapping(value = "deleteAccountByAdmin",  method = RequestMethod.GET)	//회원 조회 목록에서 삭제
   	public String deleteAccount(Model model,@RequestParam("login_id") String login_id) {

		userMapper.deleteByLoginId(login_id);




		return "redirect:userList";

	}





}
