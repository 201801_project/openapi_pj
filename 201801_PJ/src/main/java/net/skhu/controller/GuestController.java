package net.skhu.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.skhu.dto.Basic;
import net.skhu.dto.User;
import net.skhu.mapper.ArticleMapper;
import net.skhu.mapper.BasicMapper;
import net.skhu.mapper.CookingMapper;
import net.skhu.mapper.IngredientMapper;
import net.skhu.mapper.UserMapper;
import net.skhu.model.Item;
import net.skhu.model.Option;
import net.skhu.model.Pagination;
import net.skhu.model.UserRegistrationModel;
import net.skhu.service.NaverBookService;
import net.skhu.utils.Encryption;

@Controller
@RequestMapping("guest")
public class GuestController {
	@Autowired
	ArticleMapper articleMapper;
	@Autowired
	BasicMapper basicMapper;
	@Autowired
	IngredientMapper ingredientMapper;
	@Autowired
	CookingMapper cookingMapper;
	@Autowired
	UserMapper userMapper;
	 @Autowired
	NaverBookService service;




	 @RequestMapping("booksearch")
	   public String bookSearch(Model model){

	       return "book/booksearch";
	   }
	@RequestMapping("bookList")
	   public String bookList(@RequestParam("keyword")String keyword, @RequestParam("pg") int pg,Model model){	//10개씩 요리 잡지 조회
																												//페이지이동은 이전, 다음 버튼에 따라 pg 값의 변화로 출력 시작 번호가 달라짐

	       if(keyword !=null  )
	       {

	    	   List<Item> item = service.searchItem(keyword,10,(10*pg)-9);

	    	   model.addAttribute("keyword",keyword);
	    	   model.addAttribute("pg",pg);

	    	   model.addAttribute("bookList",item);
	       }

	       return "book/booklist";
	   }

	@RequestMapping(value = "dynamicSQL2")									//메뉴 '기본' 정보 조회
	public String dynamicSQL2(Pagination pagination, Model model) {
		int count = basicMapper.count(pagination);
		pagination.setRecordCount(count);

		List<Basic> list = basicMapper.findAll(pagination);
		model.addAttribute("list", list);

		Option[] orderBy = basicMapper.orderBy;
		Option[] searchBy = basicMapper.searchBy;

		model.addAttribute("orderBy", orderBy);
		model.addAttribute("searchBy", searchBy);

		return "guest/dynamicSQL2";

	}

	@RequestMapping(value = "index")
	public String index() {
		return "guest/index";
	}

	@RequestMapping(value = "login")										//로그인 페이지
	public String login() {
		return "guest/login";
	}

	@RequestMapping(value = "register", method = RequestMethod.GET)			//회원가입 양식 페이지
	public String register(UserRegistrationModel user, Model model) {
		Option[] area = UserMapper.area;
		model.addAttribute("area",area);
		return "guest/register";
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)		//회원 등록 저장
	public String register(@Valid UserRegistrationModel userRegistrationModel, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "guest/register";
		}
		User user = userRegistrationModel.toUser();							//전달받은 회원양식(pwd1,pwd2...)에서 User에 해당하는 칼럼만 받아옴.
		String pw = Encryption.encrypt(userRegistrationModel.getPasswd1(), Encryption.MD5);//암호는 Encrytion.java 코드로 암호화 해서 저장
		user.setEnabled(true);
		user.setUser_type("프로셰프");
		user.setPassword(pw);
		userMapper.save(user);
		return "redirect:/guest/success";
	}

	@RequestMapping("success")												//회원가입 성공 페이지
	public String success() {
		return "guest/success";
	}

	/*// 소개
	@RequestMapping(value = "intro")
	public String intro(Model model) {
		Article article = articleMapper.findIntro();
		model.addAttribute("article",article);
		return "guest/intro";
	}*/

	 /*// 소개 수정
    @RequestMapping(value="updateIntro",  method = RequestMethod.GET)
    public String updateIntro(Model model) {
    	Article article = articleMapper.findIntro();
    			articleMapper.updateIntro(article.getAid(), article.getContent(), article.getTitle());
    	return "guest/updateIntro";
    }

    @RequestMapping(value="updateIntro",  method = RequestMethod.POST)
    public String uadateIntro(Model model, Article article) {
		articleMapper.updateIntro(article.getAid(), article.getContent(), article.getTitle());
    	return "redirect:guest/Intro";
    }*/
}
