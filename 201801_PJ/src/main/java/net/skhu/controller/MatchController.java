package net.skhu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.skhu.dto.Calender;
import net.skhu.dto.Comment;
import net.skhu.dto.Follower;
import net.skhu.dto.Match2;
import net.skhu.mapper.AreaMapper;
import net.skhu.mapper.CalenderMapper;
import net.skhu.mapper.CommentMapper;
import net.skhu.mapper.FollowerMapper;
import net.skhu.mapper.Match2Mapper;
import net.skhu.mapper.ScheduleMapper;

@Controller
@RequestMapping("memb")
public class MatchController {
	@Autowired
	CalenderMapper calenderMapper;
	@Autowired
	Match2Mapper match2Mapper;
	@Autowired
	CommentMapper commentMapper;
	@Autowired
	FollowerMapper followerMapper;
	@Autowired
	AreaMapper areaMapper;
	@Autowired
	ScheduleMapper scheduleMapper;




	@RequestMapping(value = "calender") 											// 달력모양 게시판 보여주기
	public String calender(Model model, @RequestParam("month") int month) {


		List<Calender> list = calenderMapper.findByEntire(month);
		List<Match2> exists = match2Mapper.findExist(month);
		model.addAttribute("month", month);
		model.addAttribute("list", list);
		model.addAttribute("exists", exists);										//날짜&지역 칼럼을 기준으로 distinct로 목록을 조회해서 보냄

		return "memb/calender";

	}


	/*@RequestMapping(value = "matchCommunity") 										// 달력 날짜 클릭시 해당 날짜 게시글 조회
	public String matchCommunity(Model model, @RequestParam("board_id") int board_id, @RequestParam("date") int date) {
		List<Match2> list = match2Mapper.findByDate(board_id, date);
		model.addAttribute("list", list);

		return "memb/matchCommunity";

	}*/

	@RequestMapping(value = "match2View", method = RequestMethod.GET) 					// 게시글 상세 조회
	public String match2View(Model model, @RequestParam("id") int mid, Authentication auth) {
		Match2 match2 = match2Mapper.findOne(mid);
		List<Comment> comments = commentMapper.findOneByMatch_id(mid);
		List<Follower> flist = followerMapper.findAll(mid);


		Follower follower = new Follower(); 											// 신청란
		String vo = (String) auth.getPrincipal();										// 신청란에 채울 현재 로그인된 아이디 정보

		model.addAttribute("match2", match2);											//게시글
		model.addAttribute("comments", comments);										//댓글 목록
		model.addAttribute("comment", new Comment());									//댓글 창
		model.addAttribute("flist", flist);												//신청자
		model.addAttribute("follower", follower);										//신청란
		model.addAttribute("vo",vo);													//로그인아이디
		return "memb/match2View";
	}

	@RequestMapping(value = "follower", method = RequestMethod.POST) 					// 요리교실 신청
	public String match2View(RedirectAttributes redirectAttreibutes, Model model,Follower follower) {
		followerMapper.insert(follower);
		redirectAttreibutes.addAttribute("id", follower.getMid());
		return "redirect:match2View";
	}

	@RequestMapping(value = "comment", method = RequestMethod.POST) 					// 요리교실 댓글 달기
	public String comment(RedirectAttributes redirectAttreibutes, Model model,	Comment comment) {
		commentMapper.insertAtMatch(comment);
		redirectAttreibutes.addAttribute("id", comment.getMatch_id());
		return "redirect:match2View";
	}

	@RequestMapping(value = "area") 													// 날짜 아래 지역 버튼 눌렀을 때  해당 날짜와 지역 조건 부합하는 결과 조회
	public String area(Model model, @RequestParam("date") int date, @RequestParam("area_name") String area_name) {
		List<Match2> list = match2Mapper.findByDateAndAreaName(date, area_name);
		model.addAttribute("list", list);
		return "memb/matchCommunity";
	}

	/*@RequestMapping(value = "date")													//지역버튼이 없고 날짜칸만 클릭할 수 있는 곳은 어차피 눌러도 게시물이 없는 날짜여서 이제 사용하지 않으므로 주석처리함.
	public String date(Model model, @RequestParam("date") int date) {
		List<Match2> list = match2Mapper.findByDate(3,date);
		model.addAttribute("list", list);
		return "memb/matchCommunity";
	}*/

	@RequestMapping(value = "deleteFollower") 											// 신청취소
	public String deleteFollower(RedirectAttributes redirectAttreibutes, Model model , Authentication auth,@RequestParam("mid") int mid) {
		String vo = (String) auth.getPrincipal();										//현재 로그인된 자기 자신의 신청만 수정 가능, 다른 사람의 신청은 수정할 수 없음

		followerMapper.deleteByLoginIdAndMid(vo,mid);
		redirectAttreibutes.addAttribute("id", mid);
		return "redirect:match2View";
	}

}
