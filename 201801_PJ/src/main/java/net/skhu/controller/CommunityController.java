package net.skhu.controller;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import net.skhu.dto.Article;
import net.skhu.dto.Board;
import net.skhu.dto.File;
import net.skhu.mapper.AreaMapper;
import net.skhu.mapper.ArticleMapper;
import net.skhu.mapper.BasicMapper;
import net.skhu.mapper.BoardMapper;
import net.skhu.mapper.CalenderMapper;
import net.skhu.mapper.CookingMapper;
import net.skhu.mapper.FileMapper;
import net.skhu.mapper.IngredientMapper;
import net.skhu.mapper.Match2Mapper;
import net.skhu.mapper.TimetableMapper;
import net.skhu.mapper.UserMapper;
import net.skhu.service.UserService;

@Controller
@RequestMapping("memb")
public class CommunityController {
	@Autowired
	BasicMapper basicMapper;
	@Autowired
	IngredientMapper ingredientMapper;
	@Autowired
	CookingMapper cookingMapper;
	@Autowired
	ArticleMapper articleMapper;
	@Autowired
	UserMapper userMapper;
	@Autowired
	BoardMapper boardMapper;
	@Autowired
	UserService userService;
	@Autowired
	CalenderMapper calenderMapper;
	@Autowired
	Match2Mapper match2Mapper;
	@Autowired
	FileMapper fileMapper;
	@Autowired
	TimetableMapper timetableMapper;
	@Autowired
	AreaMapper areaMapper;

		// 커뮤니티 목록
		@RequestMapping(value = "community", method = RequestMethod.GET)
		public String community(Model model, @RequestParam("bd") int bd) {
			Board board = boardMapper.findOne(bd);
			List<Article> CommuList = articleMapper.findAll(bd);
			model.addAttribute("board", board);
			model.addAttribute("CommuList", CommuList);
			return "memb/community";
		}

	    //커뮤니티 보기
	    @RequestMapping(value="commuView")
	    public String CommuView(Model model, @RequestParam("aid") int a_id, HttpSession session) throws Exception {
	    	/*try {
	            Thread.sleep(1000);
	        } catch (InterruptedException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }*/
	    	Article CommuView = articleMapper.findArticle(a_id);
	    	List<File> files = fileMapper.findAll(a_id);
			articleMapper.updateHit(CommuView.getAid(), CommuView.getHit(), session); //조회수 증가
			model.addAttribute("CommuView", CommuView);
			model.addAttribute("files", files);
	    	return "memb/commuView";
	    }

	     //커뮤니티 생성
	    @RequestMapping(value="commuCreate", method = RequestMethod.GET)
	    public String CommuCreate(Model model, Authentication auth) {
	    	String user = (String) auth.getPrincipal();
			model.addAttribute("user",user);
	    	model.addAttribute("article", new Article());
	    	return "memb/commuEdit";
	    }

	    //커뮤니티 생성
	    @RequestMapping(value="commuCreate", method = RequestMethod.POST)
	    public String CommuCreate(Model model, Article article/*@RequestParam("fileUpload") MultipartFile[] uploadFiles*/) throws IOException{
	    	Article last = articleMapper.findTopByBoardIdOrderByNoDesc(2);
	    	Article last2 = articleMapper.findTopByBoardIdOrderByAidDesc();
			  int no = (last == null) ? 1 : last.getNo() + 1;
			  int aid = (last2 == null ) ? 1 : last2.getAid() +1;
			  Article a = new Article();
			  a.setWriter(article.getWriter());
			  a.setNo(no);
			  a.setTitle(article.getTitle());
			  a.setContent(article.getContent());
			  /*for(MultipartFile uploadFile : uploadFiles) {
		            if (uploadFile.getSize() <= 0) continue;
			            String fileName = Paths.get(uploadFile.getOriginalFilename()).getFileName().toString();
			            File uploadedFile = new File();
			            uploadedFile.setA_id(article.getAid());
			            uploadedFile.setFile_name(fileName);
			            uploadedFile.setFile_size((int)uploadFile.getSize());
			            uploadedFile.setFile_time(new Date());
			            uploadedFile.setData(uploadFile.getBytes());
			            fileMapper.insert(uploadedFile);
		        }*/
			  articleMapper.insertCommunity(a);
	    	return "redirect:commuView?aid=" + aid;
	    }

		  //커뮤니티 수정
		  @RequestMapping(value="commuEdit", method = RequestMethod.GET)
		  public String edit(Model model, @RequestParam("aid") int a_id) throws Exception {
			  try {
		            Thread.sleep(1000);
		        } catch (InterruptedException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
			  Article article = articleMapper.findArticle(a_id);
			  List<File> files = fileMapper.findAll(a_id);

			  //File file = fileMapper.findOne(a_id, f_id);
			  //model.addAttribute("file", file);
			  model.addAttribute("files", files);
			  model.addAttribute("article", article);
			  return "memb/commuEdit";
		  }

		  @Transactional
		  @RequestMapping(value="commuEdit", method = RequestMethod.POST)
		  public String edit(Model model, @Valid Article article, BindingResult bindingResult/*, @RequestParam("fileUpload") MultipartFile[] uploadFiles*/) throws IOException {
			  if (bindingResult.hasErrors()) {
				 articleMapper.findArticle(article.getAid());
				 return "memb/commuEdit?aid=" + article.getAid();
			  }
			  articleMapper.update(article);
				/*for(MultipartFile uploadFile : uploadFiles) {
		            if (uploadFile.getSize() <= 0) continue;
			            String fileName = Paths.get(uploadFile.getOriginalFilename()).getFileName().toString();
			            File uploadedFile = new File();
			            uploadedFile.setA_id(article.getAid());
			            System.out.println(article.getAid());
			            uploadedFile.setFile_name(fileName);
			            uploadedFile.setFile_size((int)uploadFile.getSize());
			            uploadedFile.setFile_time(new Date());
			            uploadedFile.setData(uploadFile.getBytes());
			            fileMapper.insert(uploadedFile);
		        }*/
			 return "redirect:commuEdit?aid=" + article.getAid();
		  }

		  //커뮤니티 삭제
		  @RequestMapping(value="commuDelete", method=RequestMethod.GET)
		  public String delete(@RequestParam("aid") int a_id, Model model) {
			  articleMapper.delete(a_id);
			  fileMapper.deleteAllFile(a_id);
			  return "redirect:community?bd=2";
		  }

		  //파일
		  	//("ROLE_PRO")
		    @RequestMapping("fileList")
		    public String fileList(Model model, @RequestParam("aid") int a_id) {
		    	try {
		            Thread.sleep(1000);
		        } catch (InterruptedException e) {
		            // TODO Auto-generated catch block
		            e.printStackTrace();
		        }
		    Article article = articleMapper.findArticle(a_id);
		    model.addAttribute("article", article);
		    model.addAttribute("files", fileMapper.findAll(a_id));
		    return "memb/fileList";

		    }

		    @RequestMapping(value="fileUpload", method=RequestMethod.POST)
		    public String fileUpload(Model model, @RequestParam("fileUpload") MultipartFile[] uploadFiles, File file) throws IOException {
		    	//article = articleMapper.findArticle();
		    	//List<File> files = fileMapper.findAll(.getA_id());
		    	//model.addAttribute("files", files);
		    	for(MultipartFile uploadFile : uploadFiles) {
		            if (uploadFile.getSize() <= 0) continue;
			            String fileName = Paths.get(uploadFile.getOriginalFilename()).getFileName().toString();
			            File uploadedFile = new File();
			            uploadedFile.setA_id(file.getA_id());
			            //System.out.println(article.getAid());
			            uploadedFile.setFile_name(fileName);
			            uploadedFile.setFile_size((int)uploadFile.getSize());
			            uploadedFile.setFile_time(new Date());
			            uploadedFile.setData(uploadFile.getBytes());
			            fileMapper.insert(uploadedFile);
		        }
		        return "redirect:commuEdit?aid=" + file.getA_id();
		    }

		    @RequestMapping("fileDelete")
		    public String fileDelete(@RequestParam("id") int id, File file) throws Exception {
		    	file = fileMapper.findOne(id);
		    	fileMapper.delete(file);
		        return "redirect:fileList?aid=" + file.getA_id();
		    }

		    @RequestMapping("fileDownload")
		    public void fileDownload(@RequestParam("id") int id, HttpServletResponse response) throws Exception {
		        File uploadedfile = fileMapper.findOne(id);
		        if (uploadedfile == null) return;
		        String file_name = URLEncoder.encode(uploadedfile.getFile_name(),"UTF-8");
		        response.setContentType("application/octet-stream");
		        response.setHeader("Content-Disposition", "attachment;filename=" + file_name + ";");
		        try (BufferedOutputStream output = new BufferedOutputStream(response.getOutputStream())) {
		            output.write(uploadedfile.getData());
		        }
		    }
}

