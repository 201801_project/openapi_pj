package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Area;


@Mapper
public interface AreaMapper {
	Area findOneByAreaId(@Param("area_id") int area_id);
	List<Area> findAll();

}
