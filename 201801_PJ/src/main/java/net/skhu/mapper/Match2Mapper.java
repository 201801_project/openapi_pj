package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Match2;
import net.skhu.model.CreateMatch2Article;
@Mapper
public interface Match2Mapper {
	List<Match2> findAll(int board_id);
	List<Match2> findAllWithComments(String login_id);
	Match2 findOne(int mid);
	Match2 findByAreaIdAndTimeId(@Param ("time_id") int timeId, @Param ("area_id") int areaId);
	List<Match2> findByLoginId(@Param ("vo") String vo);
	List<Match2> findByDate(@Param ("board_id") int board_id, @Param("date")int date);
	void insert(CreateMatch2Article createMatch2Article);
	List<Match2> findByDateAndAreaName(@Param ("date") int date, @Param("area_name")String areaName);

	List<Match2> findExist(@Param ("month") int month);

}
