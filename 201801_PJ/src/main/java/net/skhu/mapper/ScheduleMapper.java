package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Schedule;

@Mapper
public interface ScheduleMapper {

	List<Schedule> findAll();
	Schedule findOne(@Param("time_id") int timeId);

}
