package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Follower;

@Mapper
public interface FollowerMapper {
	 List<Follower> findAll(int mid);

	void insert(Follower follower);
	void deleteByLoginIdAndMid(@Param("vo") String vo, @Param("mid") int mid);

}
