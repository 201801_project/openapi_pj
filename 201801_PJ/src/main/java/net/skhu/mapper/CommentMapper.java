package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import net.skhu.dto.Comment;
@Mapper							//이거 빼먹었더니 아예 서버 돌릴때 index 타겟을 못잡았다...ㅠㅠ 404에러 원인이었
public interface CommentMapper {
	
	List<Comment> findComment(int article_id);
	List<Comment> findOneByMatch_id(int mid);


	int CntComment(int article_id);
	void insert(Comment comment);
	void insertAtMatch(Comment comment);
	void update(Comment comment);
	void delete(int id);
}