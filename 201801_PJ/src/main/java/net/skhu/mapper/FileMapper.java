package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.File;

@Mapper
public interface FileMapper {
	List<File> findAll(int a_id);
	File findOne(@Param("id") int id);
	void insert(File file);
	void update(File file);
	void delete(File file);
	void deleteAllFile(@Param("a_id") int a_id);
}
