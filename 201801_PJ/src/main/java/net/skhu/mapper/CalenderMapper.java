package net.skhu.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import net.skhu.dto.Calender;
@Mapper
public interface CalenderMapper {
	public List<Calender> findByMonth( int month);
	public List<Calender> findByEntire( int month);

	public List<Calender> findDistinctMonth();
	public List<Calender> findDistinctDate();
}
