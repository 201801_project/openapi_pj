package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import net.skhu.dto.Timetable;
import net.skhu.model.TimeCompare;
import net.skhu.model.TimeSelect;

@Mapper
public interface TimetableMapper {

	void insert(TimeSelect timeselect);

	void update(TimeSelect timeselect);


	List<Timetable> findByUserAndMon(TimeCompare compare);
	List<Timetable> findByUserAndTue(TimeCompare compare);
	List<Timetable> findByUserAndWed(TimeCompare compare);
	List<Timetable> findByUserAndThu(TimeCompare compare);
	List<Timetable> findByUserAndFri(TimeCompare compare);


}
