package net.skhu.mapper;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Article;

@Mapper
public interface ArticleMapper {
	List<Article> findAll(int board_id);
	List<Article> findCommunity(int aid);
	List<Article> findByLoginId(String login_id);
	List<Article> findAllWithComments(String login_id);
	Article findArticle(int aid);
    Article findTopByBoardIdOrderByNoDesc(int board_id);
    Article findTopByBoardIdOrderByAidDesc();
    void insertCommunity(Article article);
    void update(Article article);
    void updateHit(@Param("aid") int aid, @Param("hit") int hit,HttpSession session);
    void delete(int aid);
}
