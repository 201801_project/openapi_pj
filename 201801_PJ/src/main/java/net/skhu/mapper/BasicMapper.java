package net.skhu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.Basic;
import net.skhu.model.Option;
import net.skhu.model.Pagination;
import net.skhu.model.Select;
@Mapper
public interface BasicMapper {
	Basic findOne(int recipe_id);
    int count(Pagination pagination);
    List<Basic> findAll(Pagination pagination);

    Option[] searchBy= { new Option(0,"검색없음"), new Option(1,"메뉴이름"),
    						new Option(2,"칼로리"), new Option(3,"유형분류")};
    Option[] orderBy= { new Option(0,"메뉴번호"), new Option(1,"칼로리"),
						new Option(2,"유형이름오름차순")};


    List<Basic> findAllByNation_nm(String nation_nm);

    List<Basic> findByCalorieAndNationnm(@Param("calorie") int calorie, @Param("nation_nm")String nation_nm);
    List<Basic> findByTynmIn( List<String> tynms);

    List<Basic> findByCalorie0(Select select);
    List<Basic> findByCalorie1(Select select);
    List<Basic> findByCalorie2(Select select);
    List<Basic> findByCalorie3(Select select);
    List<Basic> findByCalorie4(Select select);
    List<Basic> findByCalorie5(Select select);
    List<Basic> findByCalorie6(Select select);

    //Calorie And Nationnm And TynmIn 다 합침
    List<Basic> findBySelect( Select select);
}
