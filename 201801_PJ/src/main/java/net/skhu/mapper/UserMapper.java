package net.skhu.mapper;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import net.skhu.dto.User;
import net.skhu.model.Option;
@Mapper
public interface UserMapper {

	List<User> findAll();
	User findOneByLoginId(String login_id);
	void save ( User user );
	 Option[] area= { new Option(1,"홍대"), new Option(2,"신촌"),
				new Option(3,"종로"), new Option(4,"수원"),
				new Option(5,"강남"), new Option(6,"일산"),
				new Option(7,"숙대")};

	User findOneByLoginIdAndArea(String login_id, int area_id);
	void update(@Param("area_id") int area_id, @Param("login_id") String login_id);
	void deleteByLoginId(@Param("login_id") String login_id);


	void updateAll(User u);



}
