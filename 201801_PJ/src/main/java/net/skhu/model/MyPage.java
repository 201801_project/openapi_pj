package net.skhu.model;
import lombok.Data;
@Data
public class MyPage {

	String login_id;
	String password;
	String name;
	String email;
	boolean enabled;
	String user_type;
	int area_id;
	String area_name;

}
