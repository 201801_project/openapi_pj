package net.skhu.model;
import lombok.Data;

@Data
public class TimeSelect {
	String user;
	String day;
	int [] notime;
}
