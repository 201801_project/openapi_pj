package net.skhu.model;
import lombok.Data;

@Data
public class Select {

	String radio1;
	int radio2;
	String [] unit;
}
