package net.skhu.dto;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class Match2 implements Serializable{
	private static final long serialVersionUID = 1L;
	int mid;
	int no;
	int board_id;
	String title;
	String content;
	String writer;
	int hit;
	Date writeTime;
	int date;
	int month;
	int area_id;
	int time_id;
	String area_name; // 조인
	String time; //조인

	List<Comment> comments; //resultMap할 때 필요함
}
