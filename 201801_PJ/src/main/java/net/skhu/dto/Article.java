package net.skhu.dto;

import java.security.Timestamp;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class Article {

	int aid;
	int no;
	int board_id;
	String title;
	String content;
	String writer;
	int hit;
	Timestamp writeTime;
	String login_id; //조인
	List<Comment> comments;		//resultMap할 때 필요함


	MultipartFile [] fileUpload;
}
