package net.skhu.dto;
import lombok.Data;

@Data
public class User {

	int id;
	String login_id;
	String password;
	String name;
	String email;
	boolean enabled;
	String user_type;
	int area_id;
}
