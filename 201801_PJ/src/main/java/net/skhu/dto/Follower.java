package net.skhu.dto;
import lombok.Data;

@Data
public class Follower {
	int fid;
	String follower_name;
	String leader;
	int mid;

}
