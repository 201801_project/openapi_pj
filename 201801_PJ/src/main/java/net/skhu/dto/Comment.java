package net.skhu.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@Data
public class Comment implements Serializable{
	private static final long serialVersionUID = 1L;
	int id;
	int article_id;
	int match_id;	//혹시 articleMapper에서 values 앞에 파라미터를 지정 안하고 다 넣는걸로 생략했다면 오류가 날 수 있음. 확인하기
	String content;
	String writer;
	Timestamp timestamp;

}
