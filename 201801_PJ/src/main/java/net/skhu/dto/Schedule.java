package net.skhu.dto;

import lombok.Data;

@Data
public class Schedule {
	int id;
	String time;
}
