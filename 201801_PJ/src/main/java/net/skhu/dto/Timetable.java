package net.skhu.dto;
import lombok.Data;

@Data
public class Timetable {

	int id;
	String user;
	String day;
	boolean one;
	boolean two;
	boolean three;
	boolean four;
	boolean five;


}
