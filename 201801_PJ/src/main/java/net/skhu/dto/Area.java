package net.skhu.dto;

import lombok.Data;

@Data
public class Area {
	int id;
	String area_name;
}
