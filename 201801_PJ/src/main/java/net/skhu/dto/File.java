package net.skhu.dto;

import java.util.Date;

import lombok.Data;

@Data
public class File {
	int id;
	int a_id;
	String file_name;
	int file_size;
	Date file_time;
	byte[] data;
}
