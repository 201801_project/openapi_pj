package net.skhu.dto;
import lombok.Data;

@Data
public class Calender {
	int id;
	int entire;	//전체 달력 35개 목록
	int month;
	int week;
	String wday;
	int date;
}
