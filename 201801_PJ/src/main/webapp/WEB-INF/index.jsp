<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
	<%@ include file="../../../include/nav.jsp" %>
	<div class="container">
		<div class="jumbotron">
			<h1 class="text-center">요리 레시피를 소개합니다.</h1>
			<p class="text-center">요리 레시피를 소개합니다. 요리를 해봅시다</p>
			<p class="text-center">
				<a class="btn btn-primary btn-lg" href="#" role="button">레시피결정하기</a>
			</p>
		</div>
		<div class="row">
			<div class="col-md-4">
				<h4>요리 레시피 커뮤니티</h4>
				<p>요리 레시피 커뮤니티는 자신이 레시피를 통해 만든 요리를 공유하는 카테고리입니다</p>
				<p>
					<a class="btn btn-default" data-target="#modal" data-toggle="modal">자세히
						알아보기</a>
				</p>
			</div>
			<div class="col-md-4">
				<h4>요리 레시피 커뮤니티</h4>
				<p>요리 레시피 커뮤니티는 자신이 레시피를 통해 만든 요리를 공유하는 카테고리입니다</p>
				<p>
					<a class="btn btn-default" href="#">자세히 알아보기</a>
				</p>
			</div>
			<div class="col-md-4">
				<h4>요리 레시피 커뮤니티</h4>
				<p>요리 레시피 커뮤니티는 자신이 레시피를 통해 만든 요리를 공유하는 카테고리입니다</p>
				<p>
					<a class="btn btn-default" href="#">자세히 알아보기</a>
				</p>
			</div>
		</div>
		<hr>
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					<span class="glyphicon-pencil"></span> &nbsp;&nbsp;공지사항
				</h3>
			</div>
			<div class="panel-body">
				<div class="media">
					<div class="media-left">
						<a href="#"><img class="media-object" src="images/.jpg"
							alt="이미지"></a>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="">레시피(테마)</a>&nbsp;<span class="badge">New</span>
						</h4>
						테마별 레시피가 추가되었습니다.
					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>