<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
  <div class="container">
	<h1>Basic SQL</h1>
	<hr />
	
	<form:form method ="get" modelAttribute="pagination">
	<form:hidden path="pg" value ="1"/>
	<span>순서</span>
	<form:select path="ob" class="form-control autosubmit"
			itemValue="value" itemLabel="label" items="${orderBy}" />
	<span>검색내용</span>
	<form:select path="sb" class="form-control ml30"
			itemValue="value" itemLabel="label" items="${searchBy}" />
	<form:input path="st" class="form-control" placeholder="검색문자열"/>
	<button type="submit" class="btn btn-defaultW">
		<i class= "glyphicon goyphicon-search"></i>검색</button>				
	<c:if test="${pagination.sb> 0 || pagination.ob>0 }">
		<a class="btn btn-default" href="list?pg=1">
		<i class="glyphicon glyphicon-ban-circle"></i>검색취소</a>
	</c:if>
	<form:select path="sz" class="form-control autosubmit">
		<form:option value="10"/> <form:option value="15"/> <form:option value="30"/>
		
	</form:select>
	</form:form>
	
	<div class=container style="background-color:white;border:1px solid gold; float:left;  witdth:300; height:300;">
		
 	<c:forEach var="basic" items="${ list }">
 	<sec:authorize access="not authenticated">  
				<div class="row" style="float:left; margin: 20px;width:300px;">
					<img src=${ basic.img_url} style="width:300px; height: 300px;">
					<div><b>음식이름</b>&nbsp;${ basic.recipe_nm_ko }</div>
					<div><b>요약</b>&nbsp;${ basic.sumry }</div>
					<div><b>유형분류</b>&nbsp;${ basic.nation_nm }</div>
					<div><b>음식분류</b>&nbsp;${ basic.ty_nm }</div>
					<div><b>칼로리</b>&nbsp;${ basic.calorie }</div>
					<div><b>상세페이지</b>&nbsp;<a href=${ basic.det_url }>연결</a></div>
				</div>
	
  </sec:authorize>
 	<sec:authorize access="authenticated">
	   <div data-url="/201801_PJ/memb/igr.do?recipe_id=${basic.recipe_id} " class="row" style="float:left; margin: 20px;width:300px;">
					<img src=${ basic.img_url} style="width:300px; height: 300px;">
					<div><b>음식이름</b>&nbsp;${ basic.recipe_nm_ko }</div>
					<div><b>요약</b>&nbsp;${ basic.sumry }</div>
					<div><b>유형분류</b>&nbsp;${ basic.nation_nm }</div>
					<div><b>음식분류</b>&nbsp;${ basic.ty_nm }</div>
					<div><b>칼로리</b>&nbsp;${ basic.calorie }</div>
					<div><b>상세페이지</b>&nbsp;<a href=${ basic.det_url }>연결</a></div>
				</div>
	</sec:authorize>
	</c:forEach>
	
	 
	</div>
	<my:pagination pageSize="${pagination.sz}" recordCount="${pagination.recordCount}" />
	 
  </div>
</body>
<%@ include file="../../../include/footer.jsp"%>
</html>
