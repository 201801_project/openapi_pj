<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<div class="modal-body" id="modal-body">
<div class="container">
  <h1>소개 ${ article.aid > 0 ? "수정" : "등록" }</h1>
  <hr />
 <form:form method="post" modelAttribute="article">  
    <div class="form-group">
      <label>제목:</label>
      <form:input path="title" class="form-control w200" />
    </div>
    <div class="form-group">
      <label>내용:</label>
      <form:input type="text" cols="40" rows="5" style="width:30%; height: 80%;" path="content" class="form-control w200" />
    </div>
    <br>
      <button type="submit" class="btn btn-primary">
        <span class="glyphicon glyphicon-ok"></span>저장</button>
       </form:form>
</div>
</div>
</html>