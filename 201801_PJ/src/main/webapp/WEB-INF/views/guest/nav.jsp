<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
				<span class="sr-only"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index">하루셰프</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="#" data-target="#modal1" data-toggle="modal">공지사항<span
						class="sr-only"></span></a></li>
				<li class="active"><a href="#" data-target="#modal2" data-toggle="modal">소개<span
						class="sr-only"></span></a></li>
				<li class="dropdown"><a href="memb/dynamicSQL2"
					class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">레시피<span
						class="caret"> </span></a>
					<ul class="dropdown-menu">
						<sec:authorize access="authenticated">
						<li><a href="${R}memb/dynamicSQL2">요리과정 검색</a></li>
						<li><a href="${R}guest/bookList">요리책 검색</a></li>
						<li><a href="${R}memb/select">사용자지정 메뉴</a></li>
						</sec:authorize>
						
						<sec:authorize access="not authenticated">
						<li><a href="${R}guest/dynamicSQL2">재료 검색</a></li>
						<li><a href="bookList?keyword=요리">요리책 검색</a></li>
						</sec:authorize>
						
				
					</ul></li>
				<li class="dropdown"><li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
					aria-haspopup="true" aria-expanded="false">커뮤니티<span
						class="caret"> </span></a>
						<ul class="dropdown-menu">
					<sec:authorize access="authenticated">
					<li><a href="${R}memb/community?bd=2">소통</a></li>
					<li><a href="timetest">공강시간 비교</a></li>
					</sec:authorize>
					<sec:authorize access="not authenticated">
					<li><a href="${R}guest/community?bd=2">소통</a></li>
					</sec:authorize>
					</ul></li>
				<sec:authorize access="authenticated">
					<li><a href="calender">달력요리교실게시판</a></li>
				</sec:authorize>
			</ul>
				<div class="form-group">
					<form class="navbar-form navbar-left" action="bookList">
						<input type="text" class="form-control" value="요리">
						<button type="submit" class="btn btn-default">검색</button>
					</form>
				</div>
				<div style="margin-top:-0.5%;">
					<sec:authorize access="not authenticated">
						<a class="btn btn-default" href="${R}guest/login">로그인</a>
					</sec:authorize>
					<sec:authorize access="not authenticated">
						<a href="register">회원가입</a> <br />
					</sec:authorize>
					<sec:authorize access="authenticated">
						<a class="btn btn-default" href="${R}memb/logout_processing">로그아웃</a>
						<!--<ul>로그인 아이디:<br><sec:authentication property="user.login_id"/></ul>-->
						<a class="btn btn-default" href="mypage">마이페이지 </a>
					</sec:authorize>
				</div>
		</div>	
	</div>
	<div class="row">
		<div class="modal" id="modal1" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						공지사항
						<button class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="text-align: center;">
						안녕하세요111
    					<br>
      					<button type="submit" class="btn btn-primary">
      					<span class="glyphicon glyphicon-ok"></span>등록</button>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="row">
		<div class="modal" id="modal2" tabindex="-1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						공지사항
							 <div>
  								<iframe id="if" src="${R}guest/intro" width="30" height="30" scrolling="no" frameborder="0"></iframe>
 							</div>
						<a class="btn btn-primary" href="#" onclick="view(${article.board_id})">
        				<i class="glyphicon glyphicon-pencil"></i> 수정</a>
						<button class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body" style="text-align: center;">
						안녕하세요<input type="text" name="content" value="${article.content}"/> 
    					<br>
      					<button type="submit" class="btn btn-primary">
      					<span class="glyphicon glyphicon-ok"></span>등록</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	function view(bd) {
		document.form.bd.value = bd;
		document.form.action="${path}/201801_PJ/guest/updateIntro";
		document.form.submit();
	}
	<script>
    function setIframeHeight(h) {
        $("#if").height(h);
    }
</script>
</nav>