<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>
</head>
<body>
	<%@ include file="../../../include/nav.jsp"%>



	<table class="table table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>login_id</th>
				<th>password</th>
				<th>name</th>
				<th>email</th>
				<th>enabled</th>
				<th>user_type</th>
				<th>area_id</th>
				<th>수정</th>
			</tr>
		</thead>
		<tbody>
			<c:set var="i" value="0" />
			<c:forEach var="user" items="${ list }">
				<c:set var="i" value="${i+1}" />
				<tr>
					<td>${ user.id }</td>
					<td>${ user.login_id }</td>
					<td>${ user.password }</td>
					<td>${ user.name }</td>
					<td>${ user.email }</td>
					<td>${ user.enabled }</td>
					<td>${ user.user_type }</td>
					<td>${ user.area_id }</td>
					<td><button type="button" data-toggle="modal"
							data-target="#myModal${i}">Open Modal</button>수정하기</td>
					<td><a class="btn btn-default" href="deleteAccountByAdmin?login_id=${user.login_id}">삭제하기</a></td>
					<!-- Modal -->

					<div class="modal fade" id="myModal${i}" role="dialog">

						<div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">×</button>
									<h4 class="modal-title">지역 선택하기</h4>
								</div>
								<div class="modal-body">
									<p>Some text in the modal.</p>

									<form:form action="updateAll" method="post" modelAttribute="u">
										<div class="form-group">
											<label>no:</label>
											<form:input path="id" class="form-control w200"
												value="${user.id}" />
											<form:errors path="id" class="error" />
										</div>

										<div class="form-group">
											<label>아이디:</label>
											<form:input path="login_id" class="form-control w200"
												value="${user.login_id}" />
											<form:errors path="login_id" class="error" />
										</div>
										<div class="form-group">
											<label>비밀번호:</label>
											<form:password path="password" class="form-control w200"
												value="${user.password}" />
											<form:errors path="password" class="error" />
										</div>

										<div class="form-group">
											<label>이름:</label>
											<form:input path="name" class="form-control w200"
												value="${user.name}" />
											<form:errors path="name" class="error" />
										</div>
										<div class="form-group">
											<label>이메일:</label>
											<form:input path="email" class="form-control w300"
												value="${user.email }" />
											<form:errors path="email" class="error" />
										</div>
										<div class="form-group">
											<label>활성여부:</label>
											<form:input path="enabled" class="form-control ml30"
												value="${user.enabled}" />
											<form:errors path="area_id" class="error" />
										</div>
										<div class="form-group">
											<label>회원등급:</label>
											<form:input path="user_type" class="form-control ml30"
												value="${user.user_type}" />
											<form:errors path="area_id" class="error" />
										</div>
										<div class="form-group">
											<label>지역:</label>
											<form:input path="area_id" class="form-control ml30"
												value="${user.area_id}" />

											<form:errors path="area_id" class="error" />
										</div>

										<button type="submit" class="btn btn-primary">
											<span class="glyphicon plyphicon-ok"> </span> 저장
										</button>

									</form:form>



								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				</tr>

			</c:forEach>
		</tbody>
	</table>




</body>
</html>
