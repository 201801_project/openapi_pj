<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<table>
		<tr>
			<td colspan="7" width="100%" bgcolor="pink"></td>
		</tr>
		<c:forEach items="${bookList}" var="b">


			<div class="row" style="float: left; margin: 20px; width:500px;">
				<img src=${ b.image} style="width: 500px; height: 350px;">
				<div>
					<b>제목</b>&nbsp;${ b.title }
				</div>
				<div>
					<b>상세페이지</b>&nbsp;<a href=${ b.link }>${b.link}연결</a>
				</div>
			</div>




		</c:forEach>
	</table>
	<div>
		<a class="btn btn-success"
			href="bookList?keyword=${keyword}&pg=${pg-1}"> <i
			class="glyphicon glyphicon-share-alt">이전</i>
		</a>
	</div>
	<div>
		<a class="btn btn-success"
			href="bookList?keyword=${keyword}&pg=${pg+1}"> <i
			class="glyphicon glyphicon-share-alt"> 다음</i>
		</a>
	</div>

</body>
</html>
