<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
<title>Insert title here</title>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
	<%
		request.setCharacterEncoding("utf-8");
		String radio1 = request.getParameter("radio1");

		String[] chk = request.getParameterValues("unit");
		
		String radio2 = request.getParameter("radio2");
		int r2 = Integer.parseInt(radio2);

	%>
	
	<div class="container center" style="max-width:90%; margin:0 auto;">  
		<!-- <br> <br> <b><font size="5" color="gray">입력 정보를
				확인하세요.</font></b> <br> <br> <font color="blue"> , <%=radio1%></font>로
		검색 결과 출력하기 <br> <br> -->
		
		<table>
			<tr>
				<td id="title">유형분류:</td>
				<td><%=radio1%></td>
			</tr>


			<tr>
				<td id="title">조리방식: </td>
				<td><%
					for (String eachmajor : chk)
						out.println(eachmajor + ",");
				%></td>
			</tr>
		</table>
	
		<div class="container" style="background-color:white;">
			<c:forEach var="basic" items="${ fc }">
				<div class="row" style="float:left; margin: 20px;width:300px;">
					<img src=${ basic.img_url} style="width:300px; height: 300px;">
					<div><b>음식이름</b>&nbsp;${ basic.recipe_nm_ko }</div>
					<div><b>요약</b>&nbsp;${ basic.sumry }</div>
					<div><b>유형분류</b>&nbsp;${ basic.nation_nm }</div>
					<div><b>음식분류</b>&nbsp;${ basic.ty_nm }</div>
					<div><b>칼로리</b>&nbsp;${ basic.calorie }</div>
					<div><b>상세페이지</b>&nbsp;<a href=${ basic.det_url }>연결</a></div>
				</div>
			</c:forEach>
		</div>
	</div>
<%@ include file="../../../include/footer.jsp" %>
</body>
</html>
