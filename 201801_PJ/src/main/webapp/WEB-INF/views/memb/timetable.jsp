<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
<title>Insert title here</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.8.1.min.js"></script>

<script type="text/javascript">
	$(document).ready(
			function() {

				// 체크 박스 모두 체크

				$("#checkAll").click(function() {

					$("input[name=notime]:checkbox").each(function() {

						$(this).attr("checked", true);

					});

				});

				// 체크 박스 모두 해제

				$("#uncheckAll").click(function() {

					$("input[name=notime]:checkbox").each(function() {

						$(this).attr("checked", false);

					});

				});

				// 체크 되어 있는 값 추출

				$("#getCheckedAll").click(function() {

					$("input[name=notime]:checked").each(function() {

						var test = $(this).val();

						console.log(test);

					});

				});

				// 서버에서 받아온 데이터 체크하기 (콤마로 받아온 경우)

				$("#updateChecked").click(
						function() {

							var splitCode = $("#splitCode").val().split(",");

							for ( var idx in splitCode) {

								$(
										"input[name=notime][value="
												+ splitCode[idx] + "]").attr(
										"checked", true);

							}

						});

				// test case

				test1();

			});

	function test1() {

		console.log("################################################");

		console.log("## test1 START");

		console.log("################################################");

		var cnt = $("input:checkbox").size();

		console.log("checkboxSize=" + cnt);

		$("input[name=notime]:checkbox").each(function() {

			var checkboxValue = $(this).val();

			console.log("checkboxValue=" + checkboxValue);

		});

		console.log("----------------------------------------------");

		$("#checkboxArea").children().each(function() {

			var checkboxValue = $(this).children(":checkbox").val();

			var text = $(this).children().eq(1).text();

			console.log(text + "=" + checkboxValue);

		});

	}
</script>
</head>

<body>
<%@ include file="../../../include/nav.jsp" %>
	<script type="text/javascript">
		function again() {
			
			window.location.href = "first";
		}
	</script>


	<input type="button" value="다시 선택하기" onclick="again();" />

	<form action="timecreate" method="post" modelAttribute="timeselect">


		<div id="test_div2">
			<style>
.time {
	border-collapse: collapse;
}

.time th, .time td {
	border: 1px solid black;
}
</style>
			유저이름 <input type="text" name="user"> 요일 <input type="text"
				name="day">
			<div id=checkboxArea">
				<table class="time">

					<tr>
						<td><input type="checkbox" id="del_id" name="notime"
							value="0">9시</td>
					</tr>
					<tr>
						<td><input type="checkbox" id="del_id" name="notime"
							value="0">10시</td>
					</tr>
					<tr>
						<td><input type="checkbox" id="del_id" name="notime"
							value="1">12시</td>
					</tr>
					<tr>
						<td><input type="checkbox" id="del_id" name="notime"
							value="1">15시</td>
					</tr>
					<tr>
						<td><input type="checkbox" id="del_id" name="notime"
							value="1">17시</td>
					</tr>

				</table>
			</div>
			<div id="buttonGroups">

				<input type="button" id="checkAll" value="check all" /> <input
					type="button" id="uncheckAll" value="uncheck all" /> <input
					type="button" id="getCheckedAll" value="get checked all" /> <input
					type="button" id="updateChecked" value="updateChecked" />

			</div>
		</div>

		<input type="submit" value="보내기">

	</form>
	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>
