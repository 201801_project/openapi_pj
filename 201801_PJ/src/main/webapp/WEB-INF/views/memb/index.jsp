<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
	<%@ include file="../../../include/nav.jsp" %>
	<!-- !PAGE CONTENT! -->
	<div class="w3-main w3-content" style="max-width:1200px;margin-top:50px">	
	<div class="container">
		<div class="jumbotron">
			<h1 class="text-center">Welcome Oneday Chef!</h1>
			<p class="text-center">Make your dishes!</p>
			<p class="text-center">
				<a class="btn btn-primary btn-lg" href="${R}memb/select" role="button">레시피결정하기</a>
			</p>
		</div>
	</div>
			
			<hr id="about">
		  <!-- About Section -->
		  <div class="w3-container w3-padding-32 w3-center">  
		    <h3>About Me, The Food Man</h3><br>
		    <img src="${pageContext.request.contextPath}/res/images/chef.jpg" alt="Me" class="w3-image" style="display:block;margin:auto" width="50%" height="30%">
		    <div class="w3-padding-32">
		      <h4><b>I am Who I Am!</b></h4>
		      <h6><i>With Passion For Real, Good Food</i></h6>
		      <p>Just me, myself and I, exploring the universe of unknownment. I have a heart of love and an interest of lorem ipsum and mauris neque quam blog. I want to share my world with you. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
		    </div>
		  </div>
				<hr>
				<br>
				<div class="slideshow-container">
				<div class="mySlides fade">
				  <div class="numbertext">1 / 3</div>
				  <img src="${pageContext.request.contextPath}/res/images/쿠킹클래스 모음.png" style="width:100%;height:300px;">
				  <div class="text">Caption Text</div>
				</div>
				
				<div class="mySlides fade">
				  <div class="numbertext">2 / 3</div>
				  <img src="${pageContext.request.contextPath}/res/images/디저트 모음.png" style="width:100%;height:300px;">
				  <div class="text">Caption Two</div>
				</div>
				
				<div class="mySlides fade">
				  <div class="numbertext">3 / 3</div>
				  <img src="${pageContext.request.contextPath}/res/images/banner.jpg" style="width:100%;height:300px;">
				  <div class="text">Caption Three</div>
				</div>
				<br>
				<div style="text-align:center">
				  <span class="dot"></span> 
				  <span class="dot"></span> 
				  <span class="dot"></span> 
				</div>
			</div>
		</div>
		<br>
		
		<script>
		var slideIndex = 0;
		showSlides();
		
		function showSlides() {
		    var i;
		    var slides = document.getElementsByClassName("mySlides");
		    var dots = document.getElementsByClassName("dot");
		    for (i = 0; i < slides.length; i++) {
		       slides[i].style.display = "none";  
		    }
		    slideIndex++;
		    if (slideIndex > slides.length) {slideIndex = 1}    
		    for (i = 0; i < dots.length; i++) {
		        dots[i].className = dots[i].className.replace(" active", "");
		    }
		    slides[slideIndex-1].style.display = "block";  
		    dots[slideIndex-1].className += " active";
		    setTimeout(showSlides, 2000); // Change image every 3 seconds
		}
		</script>
			<br>
			<br>
			
	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>