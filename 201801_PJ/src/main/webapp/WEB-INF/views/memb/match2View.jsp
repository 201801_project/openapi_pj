<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>
</head>
<body>
	<%@ include file="../../../include/nav.jsp"%>
	<div class="container">

		<h3>${ match2.title }</h3>
		<hr />

		<div id="info">
			<span>no: </span> <span>${ match2.mid }</span> <span>글쓴이: </span> <span>${ match2.writer }</span>

			<span>글쓴시각: </span> <span><fmt:formatDate
					pattern="yyyy-MM-dd " value="${ match2.writeTime }" /></span> <span>조회수:
			</span> <span>${ match2.hit }</span>
		</div>
		<hr />
		<div id="body">${ match2.content }</div>
		<hr />

		<br />
		<div>
			<span class="social social-slideshare">신청자 목록</span>
			<br>
			<c:set var="a" value="0" />
			<c:forEach var="f" items="${ flist }">

				<button type="button" class="btn btn-info">${  f.follower_name }</button>

				<c:if test="${f.follower_name eq vo }">
					<c:set var="a" value="${a+1 }" />
					<span><a href="deleteFollower?mid=${f.mid}"
						class="btn btn-danger" role="button">신청취소</a> </span>

				</c:if>
				<br>
			</c:forEach>

		</div>
<hr />
		<form:form action="comment" method="post" modelAttribute="comment">
			<div class="input-group">
				<span class="input-group-addon"><i
					class="glyphicon glyphicon-user"></i></span>
				<form:input class="form-control w200" path="writer" value="${ vo }"
					readonly="true" />
				<form:hidden path="match_id" value="${ match2.mid }" />

				<form:input class="form-control w200" path="content"
					placeholder="여기에 댓글을 입력하세요" />


				<button type="submit" class="btn btn-primary">
					<i class="icon-ok icon-white"></i> 댓글 달기
				</button>
			</div>
		</form:form>

		<div>
			<c:forEach var="comment" items="${ comments }">
				
					<button type="button" class="btn-success">${comment.writer}</button>
					<input  type="text" class="form-control" value="${comment.content}"  placeholder="Additional Info">
					
					<br>

			</c:forEach>
		</div>
		<hr>

		<c:if test="${ a == 0 }">

			<div id="follower">
				<form:form action="follower" method="post" modelAttribute="follower">
					<form:hidden path="leader" value="${ match2.writer }" />
					<form:hidden path="mid" value="${ match2.mid }" />
					<label>신청자 이름</label>
					<form:input class="form-control w200" path="follower_name"
						value="${ vo }" />

					<button type="submit" class="btn btn-primary">
						<i class="icon-ok icon-white"></i> 신청
					</button>

				</form:form>


			</div>
		</c:if>

	</div>
</body>
<%@ include file="../../../include/footer.jsp"%>
</html>