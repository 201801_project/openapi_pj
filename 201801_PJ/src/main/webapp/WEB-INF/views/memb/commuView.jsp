<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
<div class="container">
  <h1>${ board.board_name }</h1>
  <hr />
  <h3>${ CommuView.title }</h3>
  <hr />

  <div id="info">  
    <span>no: </span>
    <span>${ CommuView.no }</span>

    <span>글쓴이: </span>
    <span>${ CommuView.login_id }</span>
  
    <span>글쓴시각: </span>
    <span><fmt:formatDate pattern="yyyy-MM-dd a HH:mm:ss" value="${ CommuView.writeTime }" /></span>
  
  	<span>조회수: </span>
    <span>${ CommuView.hit }</span>
  </div>
  <hr />
  <div id="content">${ CommuView.content }</div>
	<div id="fileList">
  	<table class="table table-bordered table-condensed" style="width:auto">
	    <c:forEach var="file" items="${ files }">
	      <tr>
	        <td style="min-width:200px">${ file.file_name }</td>
	        <td class="text-right">
	          <fmt:formatNumber type = "number" value="${ file.file_size }" /> bytes</td>
	        <td>
	          <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${ file.file_time }" /></td>
	        <td>
	          <a class="btn btn-default btn-xs" href="${R}memb/fileDownload?id=${file.id}">다운로드</a>
	         
	        </td>
	      </tr>
	    </c:forEach>
  	</table>

</div>
<script>
  if (parent) {
    var h = $("div#fileList").height() + 100;
    parent.setIframeHeight(h);
  }
</script>	

  <hr />   
  <div id="buttons">
  	<!--<input type="hidden" name="aid" value="${CommuView.aid}" href="commuEdit?aid=${ CommuView.aid }">-->
    <a class="btn btn-primary" href="commuEdit?aid=${CommuView.aid}" >
        <i class="glyphicon glyphicon-pencil"></i> 수정</a>
    <a class="btn btn-danger" href="commuDelete?aid=${CommuView.aid}" data-confirm-delete>
        <i class="glyphicon glyphicon-remove"></i> 삭제</a>
    <a class="btn btn-default" href="community?bd=2">
        <i class="glyphicon glyphicon-list"></i> 목록으로</a>
    <form:form name="form" method="post">
    <input type="hidden" name="aid">
    </form:form>
  </div>
   <br />    
</div>
<%@ include file="../../../include/footer.jsp" %>
</body>
</html>

