<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>
<title>매칭매칭</title>
<style>
table {
	border-collapse: collapse;
}

td {
	border: 1px solid gray;
	width: 120px;
	height: 120px;
	vertical-align: top;
}

th {
	background-color: #dddddd;
	border: 1px solid gray;
	height: 30px;
}

td:nth-child(1), th:nth-child(1) {
	color: red;
}
</style>



</head>

<body>
	<%@ include file="../../../include/nav.jsp"%>
	<div class="container">

		<table id="match" class="table table-bordered">
			<thead>
				<tr>
					<th class="text-center" style="width: 80px;">no</th>
					<th>제목</th>
					<th style="width: 150px;">글쓴이</th>
					<th style="width: 150px;">작성일</th>
					<th style="width: 150px;">모임 날짜</th>
					<th style="width: 150px;">모임시각</th>
					<th style="width: 150px;">모임장소</th>
					<th style="width: 150px;">조회수</th>
				</tr>
			</thead>

			<tbody>
				<c:forEach var="match" items="${ list }">
					<tr data-url="match2View?id=${match.mid}">
						<td class="text-center">${ match.mid }</td>
						<td>${ match.title }</td>
						<td>${ match.writer }</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
								value="${ match.writeTime }" /></td>
						<td>${ match.month }월 ${ match.date }일</td>
						<td>${ match.time}</td>
						<td>${ match.area_name}</td>
						<td>${ match.hit }</td>
						<td><input type="submit" value="보내기"></td>
					</tr>

				</c:forEach>


			</tbody>
		</table>
	</div>
	<a href="matchCommunity?month=${match.month} ">달력게시판으로 </a>
	<br />


</body>
<%@ include file="../../../include/footer.jsp"%>
</html>



