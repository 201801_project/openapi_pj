<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:url var="R" value="/" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet" media="screen">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${R}res/common.js"></script>
<link rel="stylesheet" href="${R}res/common.css">
</head>
<body>
	<div class="container">
		<h1>게시판별 댓글 목록</h1>
		<hr />
		<div>
		<h2>커뮤니티 댓글</h2>
		<c:forEach var="article" items="${ articles }">
			 <a class="btn btn-primary" href="commuView?aid=${article.aid}" >
			 ${article.title} <small>${ article.comments.size() }개</small>
        <i class="glyphicon glyphicon-share-alt"></i> 
        	
			</a>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>id</th>
						<th>내용</th>
						<th>이름</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="comment" items="${ article.comments }">
						<tr>
							<td>${ comment.id }</td>
							<td>${ comment.content }</td>
							<td>${ comment.writer }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:forEach>
		</div>
		
		
		
		<div>
		
		<h2>요리교실 댓글</h2>
		<c:forEach var="match" items="${ matches }">
			 <a class="btn btn-success" href="match2View?id=${match.mid}" >
			 ${match.title} <small>${ match.comments.size() }개</small>
        <i class="glyphicon glyphicon-share-alt"></i> 
        	
			</a>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>id</th>
						<th>내용</th>
						<th>이름</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="comment" items="${ match.comments }">
						<tr>
							<td>${ comment.id }</td>
							<td>${ comment.content }</td>
							<td>${ comment.writer }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:forEach>
		
		</div>
	</div>
</body>
</html>