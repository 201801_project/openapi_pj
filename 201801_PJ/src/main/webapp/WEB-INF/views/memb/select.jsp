<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
	<style type="text/css">
		div { 
				border-radius: 10px; 
			}
		
		#block {
	    display: block;
	    width: 80%;
	    border: none;
	    background-color: #ac3939;
	    padding: 14px 28px;
	    font-size: 16px;
	    cursor: pointer;
	    text-align: center;
	    border-radius: 10px; 
	    box-shadow: 2px 2px gray;
	    color:white;
	    margin:0 auto;
		}
	
		#block:hover {
	    	background-color: #ddd;
	    	color: white;
}
	</style>
<title>Insert title here</title>
</head>

<body>
<%@ include file="../../../include/nav.jsp" %>
	<script type="text/javascript">
		//보이기
		function div_show() {
			var container = $('#container');
			var right = $('#right');
			    container.fadeIn();
				document.getElementById("test_div").style.display = "block";
				setTimeout(function() {
					right.fadeIn(); }, 1000)
		};
		
		//2번째 보이기
		function div_show2() {		    		
			var container2 = $('#container2');
			var right2 = $('#right2');
				container2.fadeIn();
				document.getElementById("test_div2").style.display = "block";
				setTimeout(function() {
					right2.fadeIn(); }, 1000)
		};
		
		//3번째 보이기
		function div_show3() {
			var container3 = $('#container3');
				container3.fadeIn();
			document.getElementById("test_div3").style.display = "block";
		};
		
		//4번째 보이기
		function div_show4() {
			var block = $('#block');
				block.fadeIn();
				
		};
		//숨기기
		function div_hide() {
			document.getElementById("test_div").style.display = "none";
		}
		function again() {
			window.location.href = "select";
		}
		
		/*$(document).ready(
				function (){
		    function right() {
		    	//$("radio1").click(function(){
		    	setTimeout(function() {
		    		document.getElementById("right").style.display = "block";
		    		}, 3000);
		    	document.getElementById("div_show2()").style.display = "block";
		    })
		        $("div").animate({left: '200px'});
		    }
		});*/
	</script>
	
		<div class="center" style="max-width:20%;">
			<div class="row">
			<input type="button" style="margin-left:40px;" class="zoom btn btn-default" value="레시피 결정" onclick="div_show();" />
			<button class="btn" id="img-btn" onclick="again();">
				<img class="btn-img" src="${pageContext.request.contextPath}/res/images/refresh.png"  style="width:30px;height:30px;">
			</button>
			</div>
		</div>
		
		<form action="result" method="post" modelAttribute="select">
		<div class="row" style="margin-left:25%; margin-top:10%">
			<div class="col-sm-2">
				<div style="display: none;box-shadow: 2px 2px gray;background:#ff9933; width:140px;height:230px;position:absolute;" class="container" id="container">
					<div style="display:none;margin-top:15px;margin-left:20px;color:white;" id="test_div">
						<div class="radio">
							<input type="radio" name="radio1" value="한식" checked="checked" onclick="div_show2();"> 한식
						</div>
						<div class="radio">
							<input type="radio" name="radio1" value="중국" onclick="div_show2();"> 중국  
						</div>
						<div class="radio">	
							<input type="radio" name="radio1" value="퓨전" onclick="div_show2();"> 퓨전
						</div>
						<div class="radio">	
							<input type="radio"	name="radio1" value="일본" onclick="div_show2();"> 일본
						</div>
						<div class="radio">	
							<input type="radio"	name="radio1" value="서양" onclick="div_show2();"> 서양
						</div>
						<div class="radio">	
							<input type="radio"	name="radio1" value="이탈리아" onclick="div_show2();"> 이탈리아
						</div>
						<div class="radio">
							<input type="radio"	name="radio1" value="동남아시아" onclick="div_show2();"> 동남아시아
						</div>			
					</div>
				</div>
			</div>
			
			<div class="col-sm-1" style="margin-top:90px">
			<i style="display: none;width:20px;height:20px;" class="right" id="right"></i>
			</div>

			<div class="col-sm-2">
				<div style="display:none;box-shadow: 2px 2px gray;background:#cc6600;width:140px;height:230px;position:absolute;" class="container" id="container2">
					<div style="display: none;color:white;position:left;margin-top:15px;margin-left:5px" id="test_div2">		
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="커틀렛" onclick="div_show3();"> 커틀렛
						</div>
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="밥" onclick="div_show3();"> 밥
						</div>
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="만두/면류" onclick="div_show3();"> 만두/면류
						</div>
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="구이" onclick="div_show3();"> 구이 
						</div>
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="조림" onclick="div_show3();"> 조림
						</div>
						<div class="checkbok">
							<input type="checkbox" id="del_id" name="unit" value="밑반찬/김치" onclick="div_show3();">밑반찬/김치
						</div>
						<div class="checkbok">
						<input type="checkbox" id="del_id" name="unit" value="볶음" onclick="div_show3();"> 볶음
						</div>
						<div class="checkbok">
						<input type="checkbox" id="del_id" name="unit" value="부침" onclick="div_show3();"> 부침
						</div>
					</div>	
				</div>
			</div>
			
			<div class="col-sm-1" style="margin-top:90px">
				<i style="display: none;width:20px;height:20px;" class="right" id="right2"></i>
			</div>
			
			<div class="col-sm-2">
				<div style="display:none;box-shadow: 2px 2px gray;background:#ff9933;width:140px;height:230px;position:absolute;" class="container" id="container3">
					<div style="display: none;color:white;position:left;margin-top:15px;margin-left:20px" id="test_div3">		
						<div class="radio">
							 <input type="radio" name="radio2" value=0 onclick="div_show4();"> ~100Kcal
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=1 onclick="div_show4();"> 100Kcal~
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=2 onclick="div_show4();"> 200Kcal~
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=3 onclick="div_show4();"> 300Kcal~
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=4 onclick="div_show4();"> 400Kcal~
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=5 onclick="div_show4();"> 500Kcal~
						</div>
						<div class="radio">
							 <input type="radio" name="radio2" value=6 onclick="div_show4();"> 600Kcal~
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="center" style="margin-top:15%;">
			<button type="submit" style="display:none;" id="block">완료</button>
		</div>
	</form>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>
		
</body>
<%@ include file="../../../include/footer.jsp" %>
</html>
