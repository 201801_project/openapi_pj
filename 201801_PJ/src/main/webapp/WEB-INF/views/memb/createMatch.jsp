<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>
<style>
label {
	margin-top: 10px;
}
</style>
</head>
<body>
	<%@ include file="../../../include/nav.jsp"%>
	<h2>요리교실 ${ param.id == null ? "등록" : "수정" }</h2>
	<hr />
	<div class="container">
		<form:form method="post" modelAttribute="createMatch2Article">
			<div class="form-group">

				<label>제목</label>
				<form:input class="form-control w200" path="title" />
			</div>
			<div class="form-group">

				<label>내용</label>
				<form:input class="form-control w200" path="content" />
			</div>
			<div class="form-group">

				<label>유저아이디</label>
				<form:input class="form-control w200" path="writer" value="${ vo }" readonly="true" />
			</div>
			<div class="form-group">

				<label>월</label>
				<form:select path="month" class="form-control w200">
					<form:options itemValue="month" itemLabel="month"
						items="${ calenderMonths }" />
				</form:select>

				<label>일</label>
				<form:select path="date" class="form-control w200">
					<form:options itemValue="date" itemLabel="date"
						items="${ calenderDates }" />
				</form:select>
			</div>
			<div class="form-group">
				
				<label>지역</label>
				<button type="button" class="btn-success"
						>${ area.area_name}</button>
				<form:hidden path="area_id" value ="${area.id} "/>
			

				<label>모임 시간</label>
				<form:select path="time_id" class="form-control w200">
					<form:options itemValue="id" itemLabel="time"
						items="${ schedules }" />
				</form:select>

			</div>
			<hr />

			<div>
				<button type="submit" class="btn btn-primary">
					<i class="icon-ok icon-white"></i> 저장
				</button>
				<c:if test="${ not empty param.id }">
					<a class="btn"
						href="delete.do?id=${student.id}&${pagination.queryString}"
						data-confirm="삭제하시겠습니까?"> <i class="icon-remove"></i> 삭제
					</a>
				</c:if>
				<a href="list.do?id=${ pagination.queryString }" class="btn"> <i
					class="icon-list"></i> 목록으로
				</a>
			</div>
		</form:form>
	</div>
</body>
<%@ include file="../../../include/footer.jsp"%>
</html>
