<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
<div class="container">
  <h1> 수정 </h1>
  <hr />
  <form:form method="get" modelAttribute="user" action="editarea">  
    <div class="form-group">
      <label>지역:</label>
      <form:select path="area_id" class="form-control ml30"
			itemValue="value" itemLabel="label" items="${area}" />
    </div>
    
    <div><button type="submit" class="btn btn-default">변경</button></div>
   </form:form>
</div>

	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>