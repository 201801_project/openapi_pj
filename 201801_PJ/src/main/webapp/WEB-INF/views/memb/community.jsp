<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
	<%@ include file="../../../include/nav.jsp" %>
	<div class="container">
  <h1>${ board.board_name }</h1>
	<div class="pull-right">
      <a class="btn btn-primary" href="commuCreate">
        <i class="glyphicon glyphicon-plus"></i> 새글작성</a>
    </div>    
    
	<table id="articles" class="table table-bordered">
    <thead>
      <tr>
      	<th style="width:150px;">id</th>
        <th class="text-center" style="width:80px;">no</th>
        <th>제목</th>
        <th style="width:150px;">글쓴이</th>
        <th style="width:150px;">작성일</th>
        <th style="width:150px;">조회수</th>
      </tr>
    </thead>
    <tbody>
    <sec:authorize access="authenticated">
      <c:forEach var="article" items="${ CommuList }">
	        <tr>
	        <td>${article.aid}</td>
	        <td class="text-center">${ article.no }</td>
	        <td><a href="#" onclick="view('${article.aid}')">${ article.title }</a></td>
	        <td>${ article.writer }</td>
	        <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm" value="${ article.writeTime }" /></td>
	        <td>${ article.hit }</td>    
	        </tr>  
	   </c:forEach>
      </sec:authorize>
    </tbody>
    <form:form name="form" method="post">
    <input type="hidden" name="aid">
    </form:form>
  </table>
</div>
<script>
	function view(aid) {
		document.form.aid.value = aid;
		document.form.action="${path}/201801_PJ/memb/commuView";
		document.form.submit();
	}

</script>	
	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>