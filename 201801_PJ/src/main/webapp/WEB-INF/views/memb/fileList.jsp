<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<sec:authorize access="authenticated">
<div id="fileList">
  <table class="table table-bordered table-condensed" style="width:auto">
    <c:forEach var="file" items="${ files }">
      <tr>
        <td style="min-width:200px">${ file.file_name }</td>
        <td class="text-right">
          <fmt:formatNumber type = "number" value="${ file.file_size }" /> bytes</td>
        <td>
          <fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${ file.file_time }" /></td>
        <td>
          <a class="btn btn-default btn-xs" href="${R}memb/fileDownload?id=${file.id}">다운로드</a>
          <a class="btn btn-default btn-xs" href="${R}memb/fileDelete?id=${file.id}">삭제</a>
        </td>
      </tr>
    </c:forEach>
  </table>

  <form method="post" target="iframe1" enctype="multipart/form-data" modelAttribute="file" action="${R}memb/fileUpload">
    <input type="file" name="fileUpload" style="width:600px; margin: 10px;" multiple /> <br />
    <input type="hidden" name="a_id" value="${article.aid}"/>
    <button type="submit" class="btn btn-primary">업로드</button>
  </form>  
</div>

<script>
  if (parent) {
    var h = $("div#fileList").height() + 100;
    parent.setIframeHeight(h);
  }
</script>	
</sec:authorize>
</body>
</html>

