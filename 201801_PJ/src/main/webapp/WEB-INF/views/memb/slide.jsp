<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
<style>
.right {
    transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
}

.left {
    transform: rotate(0deg);
    -webkit-transform: rotate(0deg);
}
</style>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
  <div class="container">
	<h1>요리 순서</h1>
	<hr />
	
<!-- <div style="border:1px solid gold; float:left;  witdth:300; height:300;">
		
	<table class="table table-bordered" >
		<thead>
			<tr> 
				<th>레시피 번호</th> 
				<th> 요리 순서</th> 
				<th>요리 설명</th> 
				<th> 이미지</th> 
			</tr>
		</thead>
		
		<tbody> -->
			
  <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width:50% ; margin:0 auto; ">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    <c:forEach var="cooking" items="${list}">
    	<c:set var="cn" value="${cooking.cooking_no}" />
    	<fmt:formatNumber value="${cn}" type="number"/>
    	<c:set var="i" value="0" />
		<c:choose>    	
    	<c:when test="${cn eq 1}">
      	<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      	</c:when>
      	<c:otherwise>
      	<c:set var="i" value="${i+1}" /> 
      	<li data-target="#myCarousel" data-slide-to="i"></li>
      	</c:otherwise>
      </c:choose>
    </c:forEach>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner"> 
    <c:forEach var="cooking" items="${list}">
      <c:choose> 
	      <c:when test="${cooking.cooking_no eq 1}">
	    	  <div class="item active">
	        <img src=${ cooking.image_url } style="width:100%"/>
	      	</div>
	      </c:when> 
	      <c:otherwise>
		      	<div class="item">
		        <img src=${ cooking.image_url } style="width:100%"/>
		      	</div>
	      </c:otherwise>
      </c:choose>
     </c:forEach>
    </div>
	
    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  </div>

<!--		
		<c:forEach var="cooking" items="${list}">
			<tr> 
				<td> ${ cooking.recipe_id} </td>
				<td> ${ cooking.cooking_no } </td>
				<td> ${ cooking.cooking_dc } </td>
				<td> <img src=${ cooking.image_url}></td>
				
			</tr>
		</c:forEach>
		</tbody>
	</table>
	</div>
-->	 
  
</body>
<%@ include file="../../../include/footer.jsp" %>
</html>
