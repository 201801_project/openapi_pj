<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<c:url var="R" value="/" />
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>



<title>2018년 2월</title>			
<style>
table {
	border-collapse: collapse;
}

td {
	border: 1px solid gray;
	width: 120px;
	height: 120px;
	vertical-align: top;
}

th {
	background-color: #dddddd;
	border: 1px solid gray;
	height: 30px;
}

td:nth-child(1), th:nth-child(1) {
	color: red;
}
</style>
</head>
  
<body>
	<%@ include file="../../../include/nav.jsp"%>

	<h1>2018년 ${ month }월</h1>
	<div>
		<button type="button" class="btn btn-toggle" data-toggle="modal"
			data-target="#SelectArea">게시글 추가하기</button>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="SelectArea" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">지역 선택하기</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>

					<button type="button" class="btn-success"
						data-url="createMatch?area_id=1">홍대</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=2">신촌</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=3">종로</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=4">수원</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=5">강남</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=6">일산</button>
					<button type="button" class="btn-success"
						data-url="createMatch?area_id=7">숙대</button>



				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>









	<table class="table table-bordered">
		<thead>
			<tr>
				<th>일</th>
				<th>월</th>
				<th>화</th>
				<th>수</th>
				<th>목</th>
				<th>금</th>
				<th>토</th>
			</tr>
		</thead>
		<tbody>

			<c:set var="i" value="0" />
			<c:set var="j" value="7" />

			<c:forEach items="${ list }" var="calender">
				<c:if test="${i%j == 0 }">
					<tr>
				</c:if>

				<td >
					 ${calender.date }
					<br>
				<c:forEach items="${exists}" var="exist">
						<c:if test="${exist.date == calender.date}">
							${exist.time }
							<button type="button" class="btn-success"
								data-url="area?date=${calender.date}&area_name=${exist.area_name}">${ exist.area_name}</button>

						</c:if>
					</c:forEach>
					</td>
				
				<c:if test="${i%j == j-1 }">
					</tr>
				</c:if>
				<c:set var="i" value="${i+1 }" />
			</c:forEach>


		</tbody>
	</table>

<a href="calender?month=${month-1}">이전</a>
	<a href="calender?month=${month+1}">다음</a>
	<br />

	<%@ include file="../../../include/footer.jsp"%>
</body>
</html>

