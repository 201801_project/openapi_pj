<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp"%>
</head>
<body>
	<%@ include file="../../../include/nav.jsp"%>
	<div class="container">

		<div class="row">
			<div class="col-md-4">
				<h1 class="panel-title">개인정보</h1>
				<p>loginId: ${ mypage.login_id }</p>
				<p>비밀번호: ${ mypage.password }</p>
				<p>이메일: ${mypage.email}</p>
				<p>이름: ${ mypage.name}</p>
				<p>${ area_name }</p>
			</div>
			<div class="col-md-4">
				<h1 class="panel-title">지역설정</h1>
				<p>${ area_name }</p>
				<a class="btn btn-default" href="myarea">다시 설정하기</a>
			</div>
			<div class="col-md-4">
				<h4>탈퇴하기</h4>
				<p>계정을 삭제하시겠어요?</p>
				<p>
					<a class="btn btn-default" href="deleteAccount">자세히 알아보기</a>
				</p>
			</div>

		</div>

		<div id="info"></div>
		<hr>

		<div class="row">
			<div class="col-md-4">
				<h1 class="panel-title">요리교실 개설/참여 조회</h1>
				<p>신청내역을 확인하세요</p>
				<a class="btn btn-default" href="mymatch2">관리하기</a>
			</div>
			<div class="col-md-4">
				<h4>커뮤니티 내가 쓴 글 조회</h4>
				<p>작성한 글 목록을 조회해드립니다.</p>
				<p>
					<a class="btn btn-default" href="mycommu">자세히 알아보기</a>
				</p>
			</div>
			<div class="col-md-4">
				<h4>댓글 조회</h4>
				<p>댓글을 확인하세요</p>
				<p>
					<a class="btn btn-default" href="mycomment">자세히 알아보기</a>
				</p>
			</div>
		</div>
		<div id="info"></div>
		<hr>

		<div class="row">
			<div class="col-md-4">
			<h4>시간표 생성</h4>
				<p>시간표를 입력하세요</p>
				<p>
					<a class="btn btn-default" href="timetable">자세히 알아보기</a>
				</p>
			
			
			</div>
		</div>


	</div>
	<%@ include file="../../../include/footer.jsp"%>
</body>
</html>

