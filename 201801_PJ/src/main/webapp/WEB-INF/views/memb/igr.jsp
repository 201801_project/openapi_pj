<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
<%@ include file="../../../include/nav.jsp" %>
	<div class="container">
	<h1> 재료 정보</h1>
	<hr />
	
	<table class="table table-bordered" >
		<thead>
			<tr> 
				<th>레시피 번호</th> 
				<th> 재료번호</th> 
				<th>재료 이름</th> 
				<th> 양</th> 
				<th> 유형</th> 
			</tr>
		</thead>
		
		<tbody>
		<c:forEach var="ingredient" items="${ingredients}">
			<tr data-url="slide.do?recipe_id=${ingredient.recipe_id}">  
				<td> ${ ingredient.recipe_id} </td>
				<td> ${ ingredient.irdnt_sn } </td>
				<td> ${ ingredient.irdnt_nm } </td>
				<td> ${ ingredient.irdnt_cpcty } </td>
				<td> ${ ingredient.irdnt_ty_nm } </td>
				
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
	
	</div>
		<a href="memb/dynamicSQL2" class="btn btn-info" >목록으로 </a>
</body>
<%@ include file="../../../include/footer.jsp" %>
</html>
