<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>
</head>
<body>
<div class="container">
  <h1>학생 ${ article.aid > 0 ? "수정" : "등록" }</h1>
  <hr />
  <form:form id="form1" method="post" modelAttribute="article">  
    <div class="form-group">
      <label>제목:</label>
      <form:input path="title" class="form-control w200" />
    </div>
     <div class="form-group">
      <label>글쓴이:</label>
    	<form:input path="writer" class="form-control w200" value="${ user }" readonly="true" />
    </div>
    <div class="form-group">  
      <label>내용:</label>
       <form:input path="content" class="form-control w200" />
    </div>
    <br>

    <iframe id="iframe1" src="${R}memb/fileList?aid=${article.aid}" style="width:100%; border:none;">
  	</iframe>

      <a class="btn btn-primary" onclick="save()">
        <span class="glyphicon glyphicon-ok"></span>저장</a>
      <c:if test="${ article.aid > 0 }">
        <a href="commuDelete?aid=${ article.aid }" class="btn btn-danger" data-confirm-delete>
          <i class="glyphicon glyphicon-remove"></i> 삭제</a>
      </c:if>
      <a href="community?bd=2" class="btn btn-info">목록으로</a>
    </form:form>
</div>
	<script>
    function setIframeHeight(h) {
        $("#iframe1").height(h);
    }
  </script>
	<%@ include file="../../../include/footer.jsp" %>
</body>
</html>