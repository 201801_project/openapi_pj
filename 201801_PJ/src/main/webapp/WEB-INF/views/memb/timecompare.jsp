<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../include/taglib.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../../../include/head.jsp" %>

<style>
tr.day td {
	background-color: #FE2E2E;
}
</style>
</head>

<body>
<%@ include file="../../../include/nav.jsp" %>

	<div class="container">
		<h1>시간표 정보</h1>
		<hr />
		<c:forEach var="time" items="${ mon }">
		
			${ time.user }
		</c:forEach>
		함께 공강인 시간 결과
		<table class="table table-bordered">
			<thead>
				<tr>
					<th width=30%>유저</th>
					<th>1교시</th>
					<th>2교시</th>
					<th>3교시</th>
					<th>4교시</th>
					<th>5교시</th>
				</tr>
			</thead>

			<tbody>
				<tr class="day">
					<td>월요일</td>
				
				<c:forEach var="time" items="${ mon }">

					

						<td>${ time.one }</td>
						<td>${ time.two }</td>
						<td>${ time.three }</td>
						<td>${ time.four }</td>
						<td>${ time.five }</td>

					</tr>
					<c:set var="sum1" value="${sum1 || time.one }" />
					<c:set var="sum2" value="${sum2 || time.two }" />
					<c:set var="sum3" value="${sum3 || time.three }" />
					<c:set var="sum4" value="${sum4 || time.four }" />
					<c:set var="sum5" value="${sum5 || time.five }" />
				</c:forEach>



				<tr>
					<td></td>
					<td><c:if test="${ sum1 == false }">
							<c:out value="${sum1} " />
								월요일 1교시
						</c:if></td>
					<td><c:if test="${ sum2 == false }">
							<c:out value="${sum2} " />
								월요일 2교시
						</c:if></td>
					<td><c:if test="${ sum3 == false }">
							<c:out value="${sum3} " />
								월요일 3교시
						</c:if></td>
					<td><c:if test="${ sum4 == false }">
							<c:out value="${sum4} " />
								월요일 4교시
						</c:if></td>
					<td><c:if test="${ sum5 == false }">
							<c:out value="${sum5} " />
								월요일 5교시
						</c:if></td>


				</tr>


				<c:set var="sum1" value="false" />
				<c:set var="sum2" value="false" />
				<c:set var="sum3" value="false" />
				<c:set var="sum4" value="false" />
				<c:set var="sum5" value="false" />

				<tr bgcolor=#FE9A2E;>
					<td>화요일</td>
				
				<c:forEach var="time" items="${ tue }">

					
						
						<td>${ time.one }</td>
						<td>${ time.two }</td>
						<td>${ time.three }</td>
						<td>${ time.four }</td>
						<td>${ time.five }</td>
					</tr>
					
					<c:set var="sum1" value="${sum1 || time.one }" />
					<c:set var="sum2" value="${sum2 || time.two }" />
					<c:set var="sum3" value="${sum3 || time.three }" />
					<c:set var="sum4" value="${sum4 || time.four }" />
					<c:set var="sum5" value="${sum5 || time.five }" />
				</c:forEach>
				<tr>
					<td></td>
					<td><c:if test="${ sum1 == false }">
							<c:out value="${sum1} " />
								화요일 1교시
						</c:if></td>
					<td><c:if test="${ sum2 == false }">
							<c:out value="${sum2} " />
								화요일 2교시
						</c:if></td>
					<td><c:if test="${ sum3 == false }">
							<c:out value="${sum3} " />
								화요일 3교시
						</c:if></td>
					<td><c:if test="${ sum4 == false }">
							<c:out value="${sum4} " />
								화요일 4교시
						</c:if></td>
					<td><c:if test="${ sum5 == false }">
							<c:out value="${sum5} " />
								화요일 5교시
						</c:if></td>


				</tr>

				<c:set var="sum1" value="false" />
				<c:set var="sum2" value="false" />
				<c:set var="sum3" value="false" />
				<c:set var="sum4" value="false" />
				<c:set var="sum5" value="false" />

				<tr bgcolor=#F4FA58>
					<td>수요일</td>
				
				<c:forEach var="time" items="${ wed }">


				
						<td>${ time.one }</td>
						<td>${ time.two }</td>
						<td>${ time.three }</td>
						<td>${ time.four }</td>
						<td>${ time.five }</td>
						</tr>
				
					<c:set var="sum1" value="${sum1 || time.one }" />
					<c:set var="sum2" value="${sum2 || time.two }" />
					<c:set var="sum3" value="${sum3 || time.three }" />
					<c:set var="sum4" value="${sum4 || time.four }" />
					<c:set var="sum5" value="${sum5 || time.five }" />
				</c:forEach>
				<tr>
					<td></td>
					<td><c:if test="${ sum1 == false }">
							<c:out value="${sum1} " />
								수요일 1교시
						</c:if></td>
					<td><c:if test="${ sum2 == false }">
							<c:out value="${sum2} " />
								수요일 2교시
						</c:if></td>
					<td><c:if test="${ sum3 == false }">
							<c:out value="${sum3} " />
								수요일 3교시
						</c:if></td>
					<td><c:if test="${ sum4 == false }">
							<c:out value="${sum4} " />
								수요일 4교시
						</c:if></td>
					<td><c:if test="${ sum5 == false }">
							<c:out value="${sum5} " />
								수요일 5교시
						</c:if></td>


				</tr>




				<c:set var="sum1" value="false" />
				<c:set var="sum2" value="false" />
				<c:set var="sum3" value="false" />
				<c:set var="sum4" value="false" />
				<c:set var="sum5" value="false" />
				<tr bgcolor=#82FA58>
					<td>목요일</td>
				
				<c:forEach var="time" items="${ thu }">
					
					
						<td>${ time.one }</td>
						<td>${ time.two }</td>
						<td>${ time.three }</td>
						<td>${ time.four }</td>
						<td>${ time.five }</td>
					</tr>
					
					<c:set var="sum1" value="${sum1 || time.one }" />
					<c:set var="sum2" value="${sum2 || time.two }" />
					<c:set var="sum3" value="${sum3 || time.three }" />
					<c:set var="sum4" value="${sum4 || time.four }" />
					<c:set var="sum5" value="${sum5 || time.five }" />
				</c:forEach>
				<tr>
					
					<td></td>
					<td><c:if test="${ sum1 == false }">
							<c:out value="${sum1} " />
								목요일 1교시
						</c:if></td>
					<td><c:if test="${ sum2 == false }">
							<c:out value="${sum2} " />
								목요일 2교시
						</c:if></td>
					<td><c:if test="${ sum3 == false }">
							<c:out value="${sum3} " />
								목요일 3교시
						</c:if></td>
					<td><c:if test="${ sum4 == false }">
							<c:out value="${sum4} " />
								목요일 4교시
						</c:if></td>
					<td><c:if test="${ sum5 == false }">
							<c:out value="${sum5} " />
								목요일 5교시
						</c:if></td>


				</tr>





				<c:set var="sum1" value="false" />
				<c:set var="sum2" value="false" />
				<c:set var="sum3" value="false" />
				<c:set var="sum4" value="false" />
				<c:set var="sum5" value="false" />

				<tr bgcolor=#58D3F7>
					<td>금요일</td>
				
				<c:forEach var="time" items="${ fri }">

					
						<td>${ time.one }</td>
						<td>${ time.two }</td>
						<td>${ time.three }</td>
						<td>${ time.four }</td>
						<td>${ time.five }</td>
						</tr>
					
					<c:set var="sum1" value="${sum1 || time.one }" />
					<c:set var="sum2" value="${sum2 || time.two }" />
					<c:set var="sum3" value="${sum3 || time.three }" />
					<c:set var="sum4" value="${sum4 || time.four }" />
					<c:set var="sum5" value="${sum5 || time.five }" />
				</c:forEach>
				<tr>
					<td></td>
					<td><c:if test="${ sum1 == false }">
							<c:out value="${sum1} " />
								금요일 1교시
						</c:if></td>
					<td><c:if test="${ sum2 == false }">
							<c:out value="${sum2} " />
								금요일 2교시
						</c:if></td>
					<td><c:if test="${ sum3 == false }">
							<c:out value="${sum3} " />
								금요일 3교시
						</c:if></td>
					<td><c:if test="${ sum4 == false }">
							<c:out value="${sum4} " />
								금요일 4교시
						</c:if></td>
					<td><c:if test="${ sum5 == false }">
							<c:out value="${sum5} " />
								금요일 5교시
						</c:if></td>


				</tr>

			</tbody>
		</table>


	</div>
	
	<a href="memb/dynamicSQL2" class="btn btn-info">목록으로 </a>
	<br>
<%@ include file="../../../include/footer.jsp" %>
</body>
</html>
