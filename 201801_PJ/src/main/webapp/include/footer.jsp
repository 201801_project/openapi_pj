<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<footer style="background-color: #FFAA28; color: #ffffff">
		<div class="container">
			<br>
			<div class="row">
				<div class="col-sm-3" style="text-align: center;">
					<h5>Copyright &copy; 2018</h5>
					<h5>양다연(Yang Dayeon)</h5>
					<h5>전민선(Jeon Minseon)</h5>
				</div>
				<div class="col-sm-5">
					<h4>대표자 소개</h4>
					<p>저희는 성공회대학교에서 공부를 하고 있으며, 요리 레시피 웹사이트를 만들어 웹 개발 공부를 하고 있습니다.</p>
				</div>
				<div class="col-sm-4">
				      <h3>POPULAR TAGS</h3>
				      <p>
				        <span class="w3-tag w3-black w3-margin-bottom">Travel</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">New York</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Dinner</span>
				        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Salmon</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">France</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Drinks</span>
				        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Ideas</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Flavors</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Cuisine</span>
				        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Chicken</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Dressing</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Fried</span>
				        <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Fish</span> <span class="w3-tag w3-dark-grey w3-small w3-margin-bottom">Duck</span>
				      </p>
				    </div>
				</div>
				<div class="col-sm-2">
					<h6 style="text-align: center;margin-top: -20px;">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;by 양다연
					</h6>
					<h6 style="text-align: center;">
						<span class="glyphicon glyphicon-ok"></span>&nbsp;by 전민선
					</h6>
					<br>
				</div>
			</div>
		
	</footer>
	<script src="http://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>